
# CoNLL-U Format
# https://universaldependencies.org/format.html
# ID: Word index, integer starting at 1 for each new sentence; may be a range for multiword tokens; may be a decimal number for empty nodes (decimal numbers can be lower than 1 but must be greater than 0).
# FORM: Word form or punctuation symbol.
# LEMMA: Lemma or stem of word form.
# UPOS: Universal part-of-speech tag.
# XPOS: Language-specific part-of-speech tag; underscore if not available.
# FEATS: List of morphological features from the universal feature inventory or from a defined language-specific extension; underscore if not available.
# HEAD: Head of the current word, which is either a value of ID or zero (0).
# DEPREL: Universal dependency relation to the HEAD (root iff HEAD = 0) or a defined language-specific subtype of one.
# DEPS: Enhanced dependency graph in the form of a list of head-deprel pairs.
# MISC: Any other annotation.

# CUPT Format
# http://multiword.sourceforge.net/PHITE.php?sitesig=CONF&page=CONF_04_LAW-MWE-CxG_2018___lb__COLING__rb__&subpage=CONF_45_Format_specification

import os
import block_converter
from block_converter import ERROR_TAG, WARNING_TAG

BASE_DIR_ST_1_1 = '/Users/fedja/Code/MWE/sharedtask-data/1.1/IT/'
annotated_st_1_1_files = {
    p:os.path.join(BASE_DIR_ST_1_1,'{}.cupt'.format(p)) 
    for p in ['train','dev','test']
}

def convert_test_sentence():
    import prep_art
    import conll_util
    from paisa_test_sentence import CONLL_TEST_ANNOTATED_SENTENCE
    converted = prep_art.split_prep_art(CONLL_TEST_ANNOTATED_SENTENCE)
    # conlu_sentence = block_converter.convert_block_paisa_conllu(CONLL_TEST_ANNOTATED_SENTENCE)
    print(conll_util.block_to_str(converted))

def convert_cupt_corpus(input_file_path, output_file_path):
    import conll_util
    import csv
    log_tsv_output_file = './log_conversion.tsv'
    num_errors, num_sentences, num_inconsistent, num_tokens, num_mwes,  = 0, 0, 0, 0, 0
    print('Converting {} into {}'.format(input_file_path, output_file_path))
    with open(input_file_path) as f_in, \
        open(output_file_path, 'w') as f_out, \
        open(log_tsv_output_file, 'w') as tsv_out:
        tsv_writer = csv.writer(tsv_out, delimiter='\t')                
        while True:            
            next_headers, next_block = conll_util.get_next_conll_header_and_block(f_in)
            if next_block == []:
                f_out.write('\n')
                break
            num_sentences += 1
            num_tokens += len(next_block)
            num_mwes += conll_util.count_mwe_in_cupt_sentence(next_block)
            converted_block = block_converter.convert_block_cupt_cupt(next_block)
            converted_block_str = conll_util.block_to_str(converted_block)
            if not conll_util.pass_head_consistency(converted_block):
                num_inconsistent += 1
                continue
            f_out.write('\n'.join(next_headers))
            f_out.write('\n')
            f_out.write(converted_block_str)
            f_out.write('\n\n')
            if ERROR_TAG in converted_block_str:
                num_errors += 1
                for i in range(len(next_block)):
                    tsv_row = converted_block[i] + ['⇐'] + next_block[i]
                    tsv_writer.writerow(tsv_row)
                tsv_writer.writerow([])
                if  num_errors==100:
                    break                            
    print("Num sentences: {}".format(num_sentences))
    print("Num errors: {}".format(num_errors))
    print("Num inconsisent sentences: {}".format(num_inconsistent))
    print("Num tokens: {}".format(num_tokens))
    print("Num mwes: {}".format(num_mwes))

def main_convert_from_cupt():
    # train: 
    #   sentences: 13555
    #   tokens: 360883
    #   mwe: 3254
    # 
    # dev:
    #   sentences: 917
    #   tokens: 32613
    #   mwe: 500
    #  
    # test:
    #   sentences: 1256
    #   tokens: 37293
    #   mwe: 503
    #
    # total sentences: 15728

    
    working_dir = './st_1_2_annotated/from_cupt'
    for part in ['train', 'dev', 'test']: # 'train', 'dev', 'test'
        original_1_2_annotated = annotated_st_1_1_files[part]
        output_file = os.path.join(working_dir, '{}_conllu.cupt'.format(part))    
        convert_cupt_corpus(original_1_2_annotated, output_file)


def filter_paisa_section(output_file_path, header_match):
    import gzip
    import conll_util
    import paisa_util
    from tqdm import tqdm
    from paisa_util import PAISA_CONLL_FILE, NUM_SENTENCES_IN_PAISA

    total_sentences = 0
    extracted_sentences = 0
    current_text_id, current_text_part, current_url = None, None, None
    
    pbar = tqdm(total=NUM_SENTENCES_IN_PAISA)
    pbar.set_description('Processing PAISA')
    with gzip.open(PAISA_CONLL_FILE, 'rt') as f_in, open(output_file_path, 'w') as f_out:
        while True:        
            next_header, next_block = paisa_util.get_next_paisa_header_and_block(f_in)
            if next_block == []: # EOF
                f_out.write('\n')
                break

            total_sentences += 1
            pbar.update()

            if next_header == None: # no section information
                current_text_part += 1
            else:
                current_text_id, current_url = paisa_util.get_text_id_url_from_header(next_header)
                current_text_part = 1

            if header_match not in current_url:
                continue  

            extracted_sentences += 1
            sentence_text = ' '.join([r[1] for r in next_block])
            # f_out.write('# PAISA section:{}_{} sent_id:{}_{} url:{}\n'.format(
            #     header_match, extracted_sentences, 
            #     current_text_id, current_text_part, current_url
            # ))
            f_out.write('# sent_id = {}_{}\n'.format(current_text_id, current_text_part))
            f_out.write('# text = {}\n'.format(sentence_text.strip()))        
            next_block.extend([[],[]]) # two empty lines at the end
            f_out.write(conll_util.block_to_str(next_block))            
    pbar.close()

    print("Filtered {} from {} sentences.".format(extracted_sentences, total_sentences))        

def paisa_split_sections():
    
    header_title_match = [
        ('blogolandia','blogolandia.it'), # 71869 sentences
        ('wikipedia','it.wikipedia.org'), # 8320790 sentences
        ('wikinews','it.wikinews.org') # 31688 sentences
        # 8424347 total
    ]

    for header_title, header_match in header_title_match:
        print('Filtering section: {}'.format(header_match))    
        outputCoNLL = './paisa_section_split/{}.conll'.format(header_title)    
        print('outputCoNLL: {}'.format(outputCoNLL))  
        filter_paisa_section(outputCoNLL, header_match)

def map_train_dev_test_to_paisa_id():
    from tqdm import tqdm    
    
    from collections import defaultdict
    import paisa_util
    import conll_util    
    import os
    import json

    for part, input_file in annotated_st_1_1_files.items():

        print("Processing {}".format(input_file))
        text_st_id = defaultdict(list) # text -> [st_id_1, st_id_2, ...]
        with open(input_file) as f_in:
            sent_num = 0
            while True:
                header, block = conll_util.get_next_conll_header_and_block(f_in)
                if len(block)==0:
                    break
                sent_num += 1
                text = conll_util.get_text_from_conllu(block)
                text_st_id[text].append(sent_num)
                

        paisa_info_mapping = defaultdict(list) # st_id -> [paisa_info1, paisa_info2, ...]
        pbar = tqdm(total=8424347)
        pbar.set_description('Reading sections')
        section_dir = './paisa_section_split'
        for f in os.listdir(section_dir):
            if not f.endswith('.conll'):
                continue
            f = os.path.join(section_dir, f)
            with open(f) as f_in:
                while True:
                    header, block = conll_util.get_next_conll_header_and_block(f_in)
                    if len(block)==0:
                        break
                    pbar.update()
                    info = header[0]
                    text = conll_util.get_text_from_conllu(block)
                    if text in text_st_id:
                        for st_id in text_st_id[text]:
                            paisa_info_mapping[st_id].append(info)
        pbar.close()

        output_paisa_info_mapping = {}
        unknown_mapping = 0
        previous_section_id = None
        for st_id, info_list in sorted(paisa_info_mapping.items(), key=lambda x: x[0]):
            if len(info_list)==1:
                info = info_list[0]
                previous_section_id = int(info.split()[2].split('_')[1])
                output_paisa_info_mapping[st_id] = info
            else:
                found = False
                info_with_section_id_one = []
                for info in info_list:
                    current_section_id = int(info.split()[2].split('_')[1])
                    if current_section_id == previous_section_id+1:
                        previous_section_id += 1
                        found = True
                        output_paisa_info_mapping[st_id] = info
                        break
                    elif current_section_id == 1:
                        info_with_section_id_one.append(info)
                if not found:
                    if len(info_with_section_id_one)==1:
                        # found a single element with id = 1
                        output_paisa_info_mapping[st_id] = info_with_section_id_one[0]
                        previous_section_id = 1
                    else:
                        unknown_mapping += 1
                        output_paisa_info_mapping[st_id] = info_list
            

        print("Unknown mappings: {}".format(unknown_mapping))

        with open('./st_1_1_paisa_mapping/{}_mapping.json'.format(part), 'w') as f_out:
            json.dump(output_paisa_info_mapping, f_out,
                indent=3, sort_keys=True, 
                ensure_ascii=False
            )

def extract_relevant_blocks_from_sections():
    import json
    import os
    from tqdm import tqdm
    import conll_util

    working_dir = './st_1_1_paisa_mapping'

    parts_json_mapping = {
        part: os.path.join(working_dir, '{}_mapping.json'.format(part))
        for part in ['train', 'dev', 'test']
    }

    relevant_text_ids = []

    for part, input_json_file in parts_json_mapping.items():
        print('Processing {}'.format(part))
        with open(input_json_file) as f_in:
            mapping = json.load(f_in) 
            # "num_sent in part" -> "parseme_info"
            # info:    "1": "# PAISA section:blogolandia.it_1277 sent_id:7000392_5 url:http://...",
            for paisa_info in mapping.values():
                sent_id = paisa_info.split()[3].split(':')[1] # 7000392_5
                relevant_text_ids.append(sent_id)

    pbar = tqdm(total=8424347)
    pbar.set_description('Reading sections')
    section_dir = './paisa_section_split'
    relevant_output_file = os.path.join(section_dir, 'relevant.conll')
    with open(relevant_output_file, 'w') as f_out:
        for f in ['blogolandia.conll','wikinews.conll','wikipedia.conll']:
            section_input_file = os.path.join(section_dir, f)            
            with open(section_input_file) as f_in:
                while True:
                    header, block = conll_util.get_next_conll_header_and_block(f_in)
                    if len(block)==0:
                        break
                    pbar.update()
                    header_info, header_text = header[0].strip(), header[1].strip()
                    sent_id = header_info.split()[3].split(':')[1]
                    if sent_id in relevant_text_ids:
                        output_lines = [header_info, header_text] + conll_util.block_to_line_list(block) + ['','']
                        f_out.write('\n'.join(output_lines))                        
    pbar.close()            

def main_convert_from_paisa(add_mwe_column=True):
    import json
    import os
    from tqdm import tqdm
    import conll_util
    from block_converter import convert_block_paisa_conllu
    from block_converter import ERROR_TAG, WARNING_TAG
    import prep_art
    from no_space_after import add_no_space_after

    working_dir = './st_1_1_paisa_mapping'
    output_dir = './st_1_2_annotated/from_paisa'

    parts_json_mapping = {
        part: os.path.join(working_dir, '{}_mapping.json'.format(part))
        for part in ['train', 'dev', 'test'] #['train', 'dev', 'test']
    }

    output_files = {
        part: os.path.join(output_dir, '{}_conllu.cupt'.format(part))
        for part in ['train', 'dev', 'test']
    }

    for part, input_json_file in parts_json_mapping.items():
        print('Processing {}'.format(part))
        original_cupt_file = annotated_st_1_1_files[part]
        output_file_cupt = output_files[part]        
        paisa_text_id_part_num_mapping = {}
        with open(input_json_file) as f_in:
            mapping = json.load(f_in) 
            # "num_sent in part" -> "parseme_info"
            # info:    "1": "# PAISA section:blogolandia.it_1277 sent_id:7000392_5 url:http://...",
        for num_sent, paisa_info in mapping.items():
            sent_id = paisa_info.split()[3].split(':')[1] # 7000392_5
            paisa_text_id_part_num_mapping[sent_id] = num_sent

        num_sent_conll_block = {}
        num_sent_conll_header = {}

        pbar = tqdm(total=15728)
        pbar.set_description('Processing conll sentences')
        input_file = './paisa_section_split/relevant.conll'
        with open(input_file) as f_in:
            while True:
                header, block = conll_util.get_next_conll_header_and_block(f_in)
                if len(block)==0:
                    break
                pbar.update()
                info = header[0].strip()
                sent_id = info.split()[3].split(':')[1]
                if sent_id in paisa_text_id_part_num_mapping:
                    num_sent = paisa_text_id_part_num_mapping[sent_id]
                    num_sent_conll_block[int(num_sent)] = block
                    num_sent_conll_header[int(num_sent)] = info
        pbar.close()

        pbar = tqdm(total=len(num_sent_conll_block))
        pbar.set_description('Writing cupt')
        with open(original_cupt_file) as f_in, open(output_file_cupt, 'w') as f_out:
            file_header = '# global.columns = ID FORM LEMMA UPOS XPOS FEATS HEAD DEPREL DEPS MISC PARSEME:MWE\n'
            f_out.write(file_header)
            for sent_num, block in sorted(num_sent_conll_block.items(), key=lambda x: x[0]):                
                _, orig_block = conll_util.get_next_conll_header_and_block(f_in)
                header = num_sent_conll_header[sent_num]
                #header = '# sent_id = {}'.format(header.split()[3].split(':')[1])
                header = '# source_sent_id = . . {}'.format(header.split()[3].split(':')[1])
                
                ## conversion
                converted_block = prep_art.split_prep_art(block)
                converted_block = convert_block_paisa_conllu(converted_block)
                add_no_space_after(converted_block)                
                ## end of conversion
                
                assert conll_util.pass_head_consistency(converted_block)

                ignore_tags = ['FORM', 'UPOS', 'FEATS', 'DEPREL', 'DEPS', 'MISC', 'HEAD']
                # ignoring form because sometimes we remove dashes in add_no_space_after
                assert(conll_util.compare_conllu(orig_block,converted_block, ignore_tags))
                # append mwe_annotation
                if add_mwe_column:
                    for i in range(len(converted_block)):
                        converted_block[i].append(orig_block[i][-1]) 
                text_header = '# text = {}'.format(conll_util.get_text_from_conllu(converted_block))                
                output_lines = [header, text_header] + conll_util.block_to_line_list(converted_block) + ['','']                                
                block_text = '\n'.join(output_lines)
                assert ERROR_TAG not in block_text
                f_out.write(block_text)
                pbar.update()
        pbar.close()

if __name__ == '__main__':
    # convert_test_sentence()    
    # map_train_dev_test_to_paisa_id()    
    # extract_relevant_blocks_from_sections()
    
    # main_convert_from_cupt()
    main_convert_from_paisa(add_mwe_column=True)