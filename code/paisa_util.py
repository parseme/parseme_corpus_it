import re

PAISA_CONLL_FILE = '/Users/fedja/scratch/CORPORA/PAISA/paisa.annotated.CoNLL.utf8.gz'
NUM_SENTENCES_IN_PAISA = 12297010

def get_next_paisa_header_and_block(f_in):
    header = None
    block = []
    for line in f_in:
        line_strip = line.strip()
        if line_strip.startswith('#'): # header at the beginning of the file
            continue
        if line_strip in ['', '</text>']:
            if block:
                return header, block
            else:
                continue
        if line_strip.startswith('<text'):
            # <text id="7000001" url="http://www.02blog.it/">
            assert len(block) == 0 # make sure it's a header
            assert header == None
            header = line_strip
            continue
        row = line_strip.split('\t')
        block.append(row)
        assert len(row)>0 and re.match('[0-9]',row[0][0])
    return header, block

def get_text_id_url_from_header(header):
    sent_id, url = re.findall(r'["].+?["]', header)
    sent_id = sent_id[1:-1] # remove quotation
    url = url[1:-1] # remove quotation
    return sent_id, url
