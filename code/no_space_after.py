
NO_SPACE_AFTER_TAG = 'SpaceAfter=No'

# todo: missing 
# - c' è

def add_no_space_after(block):
    import re
    from block_converter import CONLU_HEADER_INDEX
    form_index = CONLU_HEADER_INDEX['FORM']
    xpos_index = CONLU_HEADER_INDEX['XPOS']
    misc_index = CONLU_HEADER_INDEX['MISC']

    ambiguous_quotation_indexes = [i for i,r in enumerate(block) if r[form_index]=='"']

    for i, current_row in enumerate(block):

        current_form = current_row[form_index]
        
        if i > 0: 
            previous_row = block[i-1]            
            # if current is punct there should be no space before
            if re.match(r'[,;:.?!»”)\]]+',current_form):
                affected_raw = block[i-3] if i>2 and '-' in block[i-3][0] else previous_row
                # check if previous_row is inside a prep-art split
                # if so add no_space_after in split token
                affected_raw[misc_index] = NO_SPACE_AFTER_TAG
            previous_form = previous_row[form_index]
            # if current is clitic pronoun (PC) and previous ends with '-' there should be no space before
            if current_row[xpos_index] == 'PC' and re.match(r'\w+-$',previous_form): # words ending in dash (not only dash)
                previous_row[form_index] = previous_form[:-1] # remove final dash
                previous_row[misc_index] = NO_SPACE_AFTER_TAG        

        # if current ends with apostrophe or previous is [(]+ the current word should have nsp        
        if current_form.endswith("'") or re.match(r'[(]',current_form):
            if i>1 and '-' in block[i-2][0]:
                # exclude to put no_space_tag in art (inside a prep-art split)
                pass 
            else:
                current_row[misc_index] = NO_SPACE_AFTER_TAG        
        
        if re.match(r'[\[(“«]+',current_form):
            current_row[misc_index] = NO_SPACE_AFTER_TAG 

    if len(ambiguous_quotation_indexes)%2 == 0:
        # even ambiguous quotations
        for n, i in enumerate(ambiguous_quotation_indexes,1):
            open_quote = n%2==1
            if open_quote:
                affected_raw = block[i]
            else: 
                affected_raw = block[i-3] if i>2 and '-' in block[i-3][0] else block[i-1]
            affected_raw[misc_index] = NO_SPACE_AFTER_TAG         
    elif len(ambiguous_quotation_indexes) == 1:
        # single double quote in sentence
        i = ambiguous_quotation_indexes[0]
        if i==0:
            block[i][misc_index] = NO_SPACE_AFTER_TAG 
        elif i >= len(block)-2: 
            # last or one but last token
            affected_raw = block[i-3] if i>2 and '-' in block[i-3][0] else block[i-1]
            affected_raw[misc_index] = NO_SPACE_AFTER_TAG             
    else:
        # odd ambiguous quotations (always leave space)
        pass
    
