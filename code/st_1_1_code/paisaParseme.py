#! /usr/bin/env python3

#SCRIPT FOR EXTRACTING N FILES OF X SENTENCES EACH FROM Y SECTIONS OF PAISA

#import csv
import gzip
import re

sentences_per_file = 1000
file_per_header = 20
paisa_root = '/Users/fedja/scratch/CORPORA/PAISA/'
inputCoNLL = paisa_root + 'paisa.annotated.CoNLL.utf8.gz'
header_title_match = [('bloglandia','blogolandia.it'),('wikipedia','it.wikipedia.org'),('wikinews','it.wikinews.org')]

def getNextConllSentenceOrHeader(f_in):
    sentenceBlock = []
    for line in f_in:
        if line.startswith('<text'):
            return line
        if line.strip()=='':
            if sentenceBlock:
                return sentenceBlock
            continue
        row = line.strip().split('\t')
        if len(row)>0 and re.match('[0-9]',row[0][0]):
            sentenceBlock.append(row)
    return None

def fill_sentences(f_in, f_out, header_match):
    inside_header = False
    s = 0
    while True:        
        nextConllOrHeader = getNextConllSentenceOrHeader(f_in)
        if isinstance(nextConllOrHeader, list):
            if inside_header:
                nextConllOrHeader.append([])
                f_out.writelines('\t'.join(row)+'\n' for row in nextConllOrHeader)            
                s += 1
                if s == sentences_per_file:
                    return
        else:
            inside_header = header_match in nextConllOrHeader 

# main part
for header_title, header_match in header_title_match:
    print('header_match: {}'.format(header_match))    
    with gzip.open(inputCoNLL, 'rt') as f_in:
        file_index = 1
        s = [0]   
        for file_index in range(1, file_per_header+1):              
            outputCoNLL = './it_{}_{}.conll'.format(header_title, file_index)
            print('outputCoNLL: {}'.format(outputCoNLL))  
            with open(outputCoNLL, 'wt') as f_out:                                         
                fill_sentences(f_in, f_out, header_match)
