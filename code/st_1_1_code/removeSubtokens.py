#! /usr/bin/env python3

#import csv
import argparse
import sys
import os
import re

def getNextConllSentence(tsv_content):
    sentenceBlock = []
    for line in tsv_content:
        if line.strip()=='':
            if sentenceBlock:
                return sentenceBlock
            continue
        row = line.strip().split('\t')
        if len(row)>0 and re.match('[0-9]',row[0][0]):
            sentenceBlock.append(row)
    return None

#-----------------------------
# fileds in conllSentence:
# 0: rank
# 1: surface
# 3: coarse PoS (V for verbs) see http://www.corpusitaliano.it/static/documents/POS_ISST-TANL-tagset-web.pdf
# 4: fine PoS (for nsp)
#
# output should have the following fields: 
# 0: rank
# 1: surface
# 2: nsp
# 3: mwe
# 4: pos
#
# For nsp we check if the current or following surface token is a specific punctuation char  
##-----------------------------
def convertConllSentenceItalian(conllSentence):
    parsemeTsvSentence = []
    for row in conllSentence:
        rank = row[0]
        surface = row[1]
        output_row = [rank, surface, '', '']
        if row[3]=='V':
            output_row.append('V')
        # if current is clitic pronoun (PC) and previous ends with '-' join them together
        #if row[4]=="PC" and parsemeTsvSentence and parsemeTsvSentence[-1][1].endswith('-'):
        #    parsemeTsvSentence[-1][1] = parsemeTsvSentence[-1][1][:-1] + surface
        #    continue
        # if current ends with apostrophe or previous is [(]+ the current word should have nsp
        if surface.endswith("'") or re.match('[(]',surface):
            output_row[2]='nsp'
        # if current is [,;:.?!)]+ the previous word should have nsp         
        if parsemeTsvSentence and re.match('[,;:.?!)]+',row[1]):
            parsemeTsvSentence[-1][2]='nsp'           
        parsemeTsvSentence.append(output_row)
    return parsemeTsvSentence

def removeSubtokensInSentence(parsemeTsvSentence):    
    newParsemeTsvSentence = []
    to_skip = 0
    for r in parsemeTsvSentence:
        if to_skip > 0: # skip current sub-token
            to_skip -= 1
            continue
        newParsemeTsvSentence.append(r)
        rank = r[0]
        if '-' in rank:
            indexes = [int(i) for i in rank.split('-')]
            assert len(indexes)==2
            to_skip = indexes[1]-indexes[0]+1 # '3-4' -> 4-3+1=2, '3-5' -> 5-3+1=3 
    return newParsemeTsvSentence

def outputTsvSentence(parsemeTsvSentence, output_mwe_content):    
    # make sure the rank is correct
    for i in range(len(parsemeTsvSentence)):
        parsemeTsvSentence[i][0] = str(i+1)
    # add empty line at the end
    parsemeTsvSentence.append([])
    # write to output file
    output_mwe_content.writelines('\t'.join(row)+'\n' for row in parsemeTsvSentence)

def removeSubtokensInFile(inputFile, outputFile):
    with open(inputFile, 'rt') as f_in: 
        with open(outputFile, 'wt') as f_out:
            while(True):
                conllSentence = getNextConllSentence(f_in) # arrays of rows where each row is a tab separated list of fields
                if conllSentence == None:
                    return     
                conllSentence = removeSubtokensInSentence(conllSentence)
                outputTsvSentence(conllSentence, f_out)

#####################################################

inputFile = '/Users/fedja/scratch/PAISA/Parseme_IT/Pilot2 ST - Italian - Platinum V6 - Annotation.tsv'
outputFile = '/Users/fedja/scratch/PAISA/Parseme_IT/Pilot2 ST - Italian - Platinum V6 - Annotation_noSubtokens.tsv'
removeSubtokensInFile(inputFile, outputFile)
