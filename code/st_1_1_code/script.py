import sys
import os
import re
import copy
import json
import prep_art

def getNextTabSentence(tsv_content):
    sentenceBlock = []
    for line in tsv_content:
        if line.strip()=='':
            if sentenceBlock:
                return sentenceBlock
            continue
        row = line.strip().split('\t')
        if len(row)>0 and re.match('[0-9]',row[0][0]):
            sentenceBlock.append(row)
    return None

def yieldTabSentences(inputTabFile):
    with open(inputTabFile, 'rt') as inputTabFile:
        while True:
            conlluSentence = getNextTabSentence(inputTabFile)
            if conlluSentence:
                yield conlluSentence
            else:
                return

def convert_files_into_split(inputConlluFile, inputParsemeFile, outputConlluFile, outputParsemeFile):
    with open(inputConlluFile, 'rt') as inputConllu, \
        open(inputParsemeFile, 'rt') as inputParseme, \
        open(outputConlluFile, 'wt') as outputConllu, \
        open(outputParsemeFile, 'wt') as outputParseme:
        while True:
            conlluSentence = getNextTabSentence(inputConllu)
            parsemeSentence = getNextTabSentence(inputParseme)
            if conlluSentence is None:
                return     
            conlluSentenceSplit, parsemeSentenceSplit = splitPrepArtParsemeTsv(conlluSentence, parsemeSentence)
            writeTabSentence(conlluSentenceSplit, outputConllu)   
            writeTabSentence(parsemeSentenceSplit, outputParseme)                


def splitPrepArtParsemeTsv(conlluSentence, parsemeSentence):

    def isPrepArtConlluRow(row):
        return row[1].lower() in prep_art.PREP_ART_SPLIT.keys() and row[3]=='E'

    assert len(conlluSentence) == len(parsemeSentence)        
    
    # clone the two tabSentences
    conlluSentenceSplit = copy.deepcopy(conlluSentence)    
    parsemeSentenceSplit = copy.deepcopy(parsemeSentence)

    # index of prepart in sentence
    prepart_index = []
    for i in range(len(conlluSentenceSplit)):
        conlluRow = conlluSentenceSplit[i]
        # parsemeRow = parsemeSentenceSplit[i]
        # check if the surface token (field 1) is a prep art and its tag is E
        if isPrepArtConlluRow(conlluRow):
            prepart_index.append(i)            
        # if current is clitic pronoun (PC) and previous ends with '-' add nsp
        if conlluRow[4]=="PC" and i>0 and parsemeSentenceSplit[i-1][1].endswith('-'):                     
            parsemeSentenceSplit[i-1][2] = 'nsp'

    # start from end
    for i in reversed(prepart_index):
        surface = conlluSentence[i][1]
        prep, art = prep_art.split(surface)
        conlluSentenceSplit.insert(i+1, list(conlluSentenceSplit[i]))
        conlluSentenceSplit.insert(i+1, list(conlluSentenceSplit[i]))
        parsemeSentenceSplit.insert(i+1, list(parsemeSentenceSplit[i]))
        parsemeSentenceSplit.insert(i+1, list(parsemeSentenceSplit[i]))        
        conlluSentenceSplit[i][0] = '-' # to signal this is a split token
        conlluSentenceSplit[i][2:] = ['_']*8 # empty all relevant field of split-token
        conlluSentenceSplit[i+1][1:6] = [prep, prep, 'E', 'E', '_']
        conlluSentenceSplit[i+2][1:6] = [art, 'il', 'R', 'RD', '_']
        conlluSentenceSplit[i+2][7] = 'det' # det's head is prep
        parsemeSentenceSplit[i][0] = '-'  # to signal this is a split token
        parsemeSentenceSplit[i][3] = '_' # empty all relevant field of split-token
        parsemeSentenceSplit[i+1][1] = prep
        parsemeSentenceSplit[i+2][1] = art
        # remove label in art token
        parsemeSentenceSplit[i+2][3] = parsemeSentenceSplit[i+2][3].split(':')[0]

    prepart_rank = [i+1 for i in prepart_index]
    recomputeRanks(conlluSentenceSplit)
    recomputeRanks(parsemeSentenceSplit)

    # fixing indexes
    for i in range(len(conlluSentenceSplit)):
        conlluRow = conlluSentenceSplit[i]
        # don't touch dep indexes for split token
        if '-' in conlluRow[0]: 
            continue
        # fix dep index in split token
        if i>1 and '-' in conlluSentenceSplit[i-2][0]:
            conlluSentenceSplit[i][6] = conlluSentenceSplit[i-1][0]
            continue
        oldIndex, oldIndexStr  = int(conlluRow[6]), conlluRow[6]        
        if oldIndexStr=='0':
            continue
        numberOfsplitsBeforeOldIndex = len([i for i in prepart_rank if i<oldIndex])
        newIndexStr = str(oldIndex + numberOfsplitsBeforeOldIndex)
        conlluRow[6] = newIndexStr        
        # check
        oldHeadRow = [row for row in conlluSentence if row[0]==oldIndexStr][0]
        newHeadRow = [row for row in conlluSentenceSplit if row[0]==newIndexStr][0]
        if newHeadRow[1] != oldHeadRow[1] and newHeadRow[3]=='E':
            index = conlluSentenceSplit.index(newHeadRow)
            newHeadRow = conlluSentenceSplit[index-1]
        assert newHeadRow[1] == oldHeadRow[1]

    return conlluSentenceSplit, parsemeSentenceSplit

def recomputeRanks(tabSentence):
    # recompute ranks, including for token splits
    rank = 1
    for i in range(len(tabSentence)):
        if '-' in tabSentence[i][0]:
            tabSentence[i][0] = '{}-{}'.format(rank, rank+1)
        else:
            tabSentence[i][0] = str(rank)
            rank += 1

def writeTabSentence(tabSentence, output):        
    # add empty line at the end
    tabSentence.append([])
    # write to output file
    output.writelines('\t'.join(row)+'\n' for row in tabSentence)

def splitFile(file, max_size):
    file_counter = 1
    sentence_counter = 0
    conlluSentence_array = []
    with open(file, 'rt') as inputConllu:   
        while True:            
            conlluSentence = getNextTabSentence(inputConllu)            
            if conlluSentence:
                conlluSentence_array.append(conlluSentence)
                sentence_counter += 1
            if conlluSentence is None or sentence_counter==max_size:
                file_out = '{}_{}'.format(file, file_counter)
                with open(file_out, 'wt') as outputConllu:
                    for c in conlluSentence_array:
                        writeTabSentence(c, outputConllu)   
                if conlluSentence is None:
                    return
                conlluSentence_array = []
                file_counter += 1
                sentence_counter = 0

def convertTrainTest():
    train_conllu = 'train.conllu'
    train_parseme = 'train1.0-autoconvert-train1.1.parsemetsv'
    train_conllu_split = 'train-split.conllu'
    train_parseme_split = 'train1.0-autoconvert-train1.1-split.parsemetsv'

    test_conllu = 'test.conllu'
    test_parseme = 'test1.0-autoconvert-dev1.1.parsemetsv'
    test_conllu_split = 'test-split.conllu'
    test_parseme_split = 'test1.0-autoconvert-dev1.1-split.parsemetsv'

    convert_files_into_split(train_conllu, train_parseme, train_conllu_split, train_parseme_split)
    convert_files_into_split(test_conllu, test_parseme, test_conllu_split, test_parseme_split)
    splitFile(train_parseme_split, 2650)

def convert_mwe_into_parsemetsv(inputFile, outputFile):
    with open(inputFile, 'rt') as input, open(outputFile, 'wt') as output:
        while True:
            line = input.readline()
            if not line: 
                break
            line = line.strip()
            if line=='':
                output.write('\n')            
            else:
                fields = [f.strip() for f in line.split('\t')][0:3]                
                fields.extend(['_','_'])                
                fields = fields[0:4]
                if fields[2]=='':
                    fields[2]='_'
                assert len(fields)==4
                output.write('\t'.join(fields) + '\n')            

def convertAll():
    parent_dir = '/Users/fedja/scratch/PAISA/Parseme_IT/'
    conllu_dir = parent_dir + 'conllU/'
    mwe_dir = parent_dir + 'mwe/' #29	battezzato	nsp		V
    split_conllu_dir = parent_dir + 'conllU_split/'
    parsemetsv_dir = parent_dir + 'parsemetsv/' #29	battezzato	nsp _
    split_parsemetsv_dir = parent_dir + 'parsemetsv_split/' #29	battezzato	nsp _ (with split)
    filenames = [x.split('.')[0] for x in os.listdir(conllu_dir) if not x.startswith('.')] # ['it_wikinews_1', ... 'it_blog_20', 'it_wikipedia_20']
    for f in filenames:
        mwe_file = '{}{}.mwe.tsv'.format(mwe_dir, f)
        parseme_file = '{}{}.parsemetsv'.format(parsemetsv_dir, f)
        conllu_file = '{}{}.conll'.format(conllu_dir, f)
        split_conllu_file = '{}{}-split.conll'.format(split_conllu_dir, f)
        split_parseme_file = '{}{}-split.parsemetsv'.format(split_parsemetsv_dir, f)
        convert_mwe_into_parsemetsv(mwe_file, parseme_file)    
        convert_files_into_split(conllu_file, parseme_file, split_conllu_file, split_parseme_file)

##############################
# MAIN
##############################

if __name__ == '__main__':	
    pass
    #convertTrainTest()
	#convertAll()
    #train_conllu = "/Users/fedja/GDrive/CloudProjects/MWE/Shared Tasks/Edition1.0/PARSEME_SharedTask2/train_test_conllu_split/it_train-split.conllu"
    #splitFile(train_conllu, 2650)
