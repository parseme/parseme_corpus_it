#! /usr/bin/env python3

import sys
import os
import re

def checkCompatibility(conllFile, platinumFile):
    with open(conllFile, 'rt') as file1:
        with open(platinumFile, 'rt') as file2:            
            lines1 = file1.readlines()
            lines2 = file2.readlines()
            if len(lines1)!=len(lines2):
                return "The two files differ in the number of lines"
            for lineNumber in range(0,len(lines1)):
                line1_fields = lines1[lineNumber].split('\t')
                line2_fields = lines2[lineNumber].split('\t')
                if line1_fields[0:2] != line1_fields[0:2]:
                    return "Diff in line {}".format(lineNumber+1)
            return "All Ok"
                

#####################################################

conllFile = '/Users/fedja/scratch/PAISA/Parseme_IT/Pilot2 ST - Italian.conll'
platinumFile = '/Users/fedja/scratch/PAISA/Parseme_IT/Pilot2 ST - Italian - Platinum V6 - Annotation - CONLL Compatible.tsv'
print(checkCompatibility(conllFile, platinumFile))

