#! /usr/bin/env python3

#import csv
import argparse
import sys
import os
import re

def getNextConllSentence(tsv_content):
    sentenceBlock = []
    for line in tsv_content:
        if line.strip()=='':
            if sentenceBlock:
                return sentenceBlock
            continue
        row = line.strip().split('\t')
        if len(row)>0 and re.match('[0-9]',row[0][0]):
            sentenceBlock.append(row)
    return None

def addUnderscoresInSentence(parsemeTsvSentence):    
    for tab_sentence in parsemeTsvSentence:
        underscores_to_add = 10 - len(tab_sentence)
        for i in range(underscores_to_add):
            tab_sentence.append('_')

def outputTsvSentence(parsemeTsvSentence, output_mwe_content):    
    # make sure the rank is correct
    for i in range(len(parsemeTsvSentence)):
        parsemeTsvSentence[i][0] = str(i+1)
    # add empty line at the end
    parsemeTsvSentence.append([])
    # write to output file
    output_mwe_content.writelines('\t'.join(row)+'\n' for row in parsemeTsvSentence)

def addUnderscores(inputFile, outputFile):
    with open(inputFile, 'rt') as f_in: 
        with open(outputFile, 'wt') as f_out:
            while(True):
                conllSentence = getNextConllSentence(f_in) # arrays of rows where each row is a tab separated list of fields
                if conllSentence == None:
                    return     
                addUnderscoresInSentence(conllSentence)
                outputTsvSentence(conllSentence, f_out)

#####################################################

inputDir = '/Users/fedja/scratch/PAISA/Parseme_IT/conll/'
outputDir = '/Users/fedja/scratch/PAISA/Parseme_IT/conllU/'

import glob
os.chdir(inputDir)
for fileName in glob.glob("*.conll"):
    inputFile = inputDir + fileName
    outputFile = outputDir + fileName
    #print("convert {} into {}".format(inputFile, outputFile))
    addUnderscores(inputFile, outputFile)    

'''
fileTest = inputDir + "it_blog_1.conll"
with open(fileTest, 'rt') as f_in: 
    conllSentence = getNextConllSentence(f_in)
    print(conllSentence)
'''