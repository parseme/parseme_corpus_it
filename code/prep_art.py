PREP_ART_SPLIT = {
    "del": ["di","il"],
    "dello": ["di","lo"], 
    "della": ["di","la"], 
    "dell'": ["di","l'"],     
    "dei": ["di","i"], 
    "degli": ["di","gli"], 
    "delle": ["di","le"], 

    "al": ["a","il"], 
    "allo": ["a","lo"],
    "alla": ["a","la"],     
    "all'": ["a","l'"], 
    "ai": ["a","i"], 
    "agli": ["a","gli"], 
    "alle": ["a","le"], 

    "dal": ["da","il"], 
    "dallo": ["da","lo"], 
    "dalla": ["da","la"], 
    "dall'": ["da","l'"], 
    "dai": ["da","i"], 
    "dagli": ["da","gli"], 
    "dalle": ["da","le"], 

    "nel": ["in","il"], 
    "nello": ["in","lo"], 
    "nella": ["in","la"], 
    "nell'": ["in","l'"], 
    "nei": ["in","i"], 
    "negli": ["in","gli"], 
    "nelle": ["in","le"], 

    "col": ["con","il"], 
    "collo": ["con","lo"], 
    "colla": ["con","la"], 
    "coi": ["con","i"], 
    "cogli": ["con","gli"], 
    "colle": ["con","le"], 

    "sul": ["su","il"], 
    "sullo": ["su","lo"], 
    "sulla": ["su","la"], 
    "sull'": ["su","l'"], 
    "sui": ["su","i"], 
    "sugli": ["su","gli"], 
    "sulle": ["su","le"], 
}

def is_prep_art(surface_form, pos):
    return surface_form.lower() in PREP_ART_SPLIT.keys() and pos=='E'

def split(prep_art):
    assert prep_art.lower() in PREP_ART_SPLIT
    result = list(PREP_ART_SPLIT[prep_art.lower()])
    if prep_art.istitle():
        result[0] = result[0].title()
    elif prep_art.isupper():
        result[0] = result[0].upper()
        result[1] = result[1].upper()
    return result

def split_prep_art(conll_block):
    from block_converter import CONLL_HEADER_INDEX
    result_block = []
    id_count = 0
    old_new_index_mapping = {}
    for row in conll_block:
        surface_form = row[1]
        pos = row[3] # pos_index = 3
        if is_prep_art(surface_form, pos):            
            prep_art_row = row[:]
            prep_art_head = row[6]
            prep_art_deprel = row[7]
            prep_art_row[0] = '{}-{}'.format(id_count+1,id_count+2)
            prep_art_row[2:] = ['_'] * 6 # empty all relevant field of split-token
            result_block.append(prep_art_row)
            prep, art = split(surface_form)
            prep_row = [str(id_count+1), prep, prep, 'E', 'E', '_', prep_art_head, prep_art_deprel]
            result_block.append(prep_row)
            art_head_starred = '*' + str(id_count+1) # det's head is prep - correct index -> starred not to be touched later
            art_row = [str(id_count+2), art, 'il', 'R', 'RD', '_', art_head_starred, 'det']
            result_block.append(art_row)
            old_new_index_mapping[row[0]] = str(id_count+1) # dependent of prep_art will be dep of prep
            id_count += 2
        else:
            id_count += 1
            new_row = row[:]            
            new_row[0] = str(id_count)
            result_block.append(new_row)
            old_new_index_mapping[row[0]] = str(id_count)
    # fixing head indexes
    for row in result_block:
        if row[6][0] == '*':
            # art_head_starred above, leave untouched but remove star
            row[6] = row[6][1:]
        elif row[6] not in ['_', '0']:
            if row[6] not in old_new_index_mapping:
                continue # inconsistent (should not be included)
            else:
                row[6] = old_new_index_mapping[row[6]]           
    return result_block