
CONLL_TEST_ANNOTATED_SENTENCE = \
'''\
1	Già	già	B	B	_	2	mod
2	oggi	oggi	B	B	_	5	mod_temp
3	la	il	R	RD	num=s|gen=f	5	det
4	gran	gran	A	A	num=s|gen=n	5	mod
5	parte	parte	S	S	num=s|gen=f	15	subj
6	delle	di	E	EA	num=p|gen=f	5	comp
7	sementi	semente	S	S	num=p|gen=f	6	prep
8	in	in	E	E	_	7	comp
9	circolazione	circolazione	S	S	num=s|gen=f	8	prep
10	,	,	F	FF	_	11	punc
11	transegenici	transegenici	A	A	num=p|gen=m	7	mod
12	e	e	C	CC	_	5	con
13	non	non	B	BN	_	15	neg
14	,	,	F	FF	_	13	punc
15	sono	essere	V	V	num=p|per=3|mod=i|ten=p	0	ROOT
16	ibridi	ibrido	S	S	num=p|gen=m	15	pred
17	che	che	P	PR	num=n|gen=n	20	subj
18	volutamente	voluto	B	B	_	20	mod
19	vengono	venire	V	VA	num=p|per=3|mod=i|ten=p	20	modal
20	resi	rendere	V	V	num=p|mod=p|gen=m	16	mod_rel
21	sterili	sterile	A	A	num=p|gen=n	20	pred
22	al	al	E	EA	num=s|gen=m	20	comp
23	solo	solo	A	A	num=s|gen=m	24	mod
24	fine	fine	S	S	num=s|gen=m	22	prep
25	di	di	E	E	_	24	arg
26	obbligare	obbligare	V	V	mod=f	25	prep
27	i	il	R	RD	num=p|gen=m	28	det
28	contadini	contadino	S	S	num=p|gen=m	26	obj
29	a	a	E	E	_	26	arg
30	comprare	comprare	V	V	mod=f	29	prep
31	le	il	R	RD	num=p|gen=f	32	det
32	sementi	semente	S	S	num=p|gen=f	30	obj
33	e	e	C	CC	_	20	con
34	non	non	B	BN	_	35	neg
35	essere	essere	V	V	mod=f	20	conj
36	autosufficienti	autosufficiente	A	A	num=p|gen=n	35	pred
37	.	.	F	FS	_	15	punc'''

# '''\
# 1	Gli	il	R	RD	num=p|gen=m	2	det
# 2	stati	stati	S	S	num=p|gen=m	4	subj
# 3	membri	membro	S	S	num=p|gen=m	2	mod
# 4	provvedono	provvedere	V	V	num=p|per=3|mod=i|ten=p	0	ROOT
# 5	affinché	affinché	C	CS	_	4	mod
# 6	il	il	R	RD	num=s|gen=m	7	det
# 7	gestore	gestore	S	S	num=s|gen=m	9	subj_pass
# 8	sia	essere	V	VA	num=s|per=3|mod=c|ten=p	9	aux
# 9	obbligato	obbligare	V	V	num=s|mod=p|gen=m	5	sub
# 10	a	a	E	E	_	9	arg
# 11	trasmettere	trasmettere	V	V	mod=f	10	prep
# 12	all'	a	E	EA	num=s|gen=n	11	comp_ind
# 13	autorità	autorità	S	S	num=n|gen=f	12	prep
# 14	competente	competente	A	A	num=s|gen=n	13	mod
# 15	una	una	R	RI	num=s|gen=f	16	det
# 16	notifica	notifica	S	S	num=s|gen=f	11	obj
# 17	entro	entro	E	E	_	11	comp_temp
# 18	i	il	R	RD	num=p|gen=m	20	det
# 19	seguenti	seguente	A	A	num=p|gen=n	20	mod
# 20	termini	termine	S	S	num=p|gen=m	17	prep
# 21	.	.	F	FS	_	4	punc'''

CONLL_TEST_ANNOTATED_SENTENCE = [x.split() for x in CONLL_TEST_ANNOTATED_SENTENCE.split('\n')]