PRINT_WARNINGS = False
ERROR_TAG = '##????##'
WARNING_TAG = '##!!!!##'

CONLL_HEADERS =  'ID FORM LEMMA CPOSTAG POSTAG FEATS HEAD DEPREL'.split()
CONLU_HEADERS = 'ID FORM LEMMA UPOS XPOS FEATS HEAD DEPREL DEPS MISC'.split()
CUPT_HEADERS =  'ID FORM LEMMA UPOS XPOS FEATS HEAD DEPREL DEPS MISC PARSEME:MWE'.split()

CONLL_HEADER_INDEX = {h:CONLL_HEADERS.index(h) for h in CONLL_HEADERS}
CONLU_HEADER_INDEX = {h:CONLU_HEADERS.index(h) for h in CONLU_HEADERS}
CUPT_HEADER_INDEX = {h:CUPT_HEADERS.index(h) for h in CUPT_HEADERS}


FINE_POS_UPOS_MAPPING = {
    'BN': 'ADV',
    'B': 'ADV',
    'CC': 'CCONJ',
    'CS': 'SCONJ',
    'E': 'ADP',
    'EA': 'ADP',
    'SP': 'PROPN', # overlapping with COARSE_POS
    'VA': 'AUX',
    'VM': 'AUX'
}

COARSE_POS_UPOS_MAPPING = {
    'A': 'ADJ',
    'D': 'DET',
    'F': 'PUNCT',
    'I': 'INTJ',
    'N': 'NUM',
    'P': 'PRON',
    'R': 'DET',
    'S': 'NOUN', # overlapping with FINE_POS    
    'T': 'DET',
    'V': 'VERB',
    'X': 'SYM'
}

FEATS_MAPPING = {
    'gen=f': 'Gender=Fem',
    'gen=o': 'Gender=Fem', # Prima, Anteprima
    'gen=m': 'Gender=Masc',
    'gen=3': 'Gender=Masc', # tratti
    'gen=n': None,
    'mod=c': 'Mood=Sub',
    'mod=d': 'Mood=Cnd',
    'mod=f': 'VerbForm=Inf',
    'mod=g': 'VerbForm=Ger',
    'mod=i': 'Mood=Ind',
    'mod=m': 'Mood=Imp',
    'mod=p': 'VerbForm=Part',
    'num=p': 'Number=Plur',
    'num=s': 'Number=Sing',
    'num=f': 'Number=Sing', # Prima, Anteprima
    'num=n': None, 
    'per=1': 'Person=1',
    'per=2': 'Person=2',
    'per=3': 'Person=3',
    'ten=f': 'Tense=Fut',
    'ten=i': 'Tense=Imp',
    'ten=p': 'Tense=Pres',
    'ten=r': 'Tense=Past',
    'ten=s': 'Tense=Past',
}

FINE_POS_FEATS_MAPPING = {
    'AP': ['Poss=Yes'],
    'BN': ['PronType=Neg'],
    'DD': ['PronType=Dem'],
    'DE': ['PronType=Exc'],
    'DI': ['PronType=Ind'],
    'DQ': ['PronType=Int'],
    'DR': ['PronTypr=Rel'],
    'SW': ['Foreign=Yes'],    
    'NO': ['NumType=Ord'],
    'PC': ['PronType=Clit'],
    'PD': ['PronType=Dem'],
    'PE': ['PronType=Prs'],
    'PI': ['PronType=Ind'],
    'PP': ['PronType=Prs', 'Poss=Yes'],
    'PQ': ['PronType=Int'],
    'PR': ['PronType=Rel'],
    'RD': ['PronType=Art', 'Definite=Def'],
    'RI': ['PronType=Art', 'Definite=Ind'],
    'SA': ['Abbr=Yes']
}

DEPREL_MAPPING = {
 'aux': 'aux',
 'clit': 'expl',
 'comp_loc': 'nmod',
 'comp_temp': 'nmod',
 'comp': 'nmod',
 'con': 'cc',
 'concat': 'flat',
 'conj': 'conj',
 'det': 'det',
 'dis': 'cc',
 'disj': 'conj',
 'mod_rel': 'acl:relcl',
 'modal': 'aux',
 'neg': 'advmod',
 'obj': 'obj',
 'pred_loc': 'nmod',
 'pred_temp': 'nmod',
 'punc': 'punct',
 'sub': 'mark',
 'subj_pass': 'nsubj:pass',
 'subj': 'nsubj',
}
    

def get_upos(cpos, fpos):
    if cpos=='_' and fpos == '_':
        return '_'
    if fpos in FINE_POS_UPOS_MAPPING:
            return FINE_POS_UPOS_MAPPING[fpos]    
    if cpos in COARSE_POS_UPOS_MAPPING:
        return COARSE_POS_UPOS_MAPPING[cpos]    
    return '{} (cpos:{},fpos:{})'.format(ERROR_TAG, cpos, fpos)
    

def get_ufeats(cpos, fpos, feats):    
    result = []
    if fpos in FINE_POS_FEATS_MAPPING:
        result.extend(FINE_POS_FEATS_MAPPING[fpos])
    if feats != '_':
        for f in feats.split('|'):
            new_f = FEATS_MAPPING[f] # should cover them all
            if new_f is None: # e.g., gen=n, num=n
                continue
            result.append(new_f)
    if result:
        feat_lower_original_case_map = {f.lower():f for f in result}
        result_str = '|'.join(
            f_original_case for f_lower,f_original_case in 
            sorted(feat_lower_original_case_map.items(), key=lambda x: x[0])
        )
        return result_str
    return '_'


def get_udeprel_1_N_arg(lemma, head_deprel, cpos, head_cpos, form_feats_key_values, head_feats_key_values):
    if cpos in ['E', 'C', 'P', 'S', 'N', 'D', 'X', 'B', 'R', 'T']: result = 'mark'
    elif cpos == 'F': result ='punct'
    elif cpos == 'I': result = 'discourse'
    elif cpos == 'A': result = 'amod' if head_cpos!='V' else 'cop' if lemma=='essere' else 'nmod'
    elif cpos == 'V':        
        mod = form_feats_key_values.get('mod', None)
        mod_is_f_g = mod in ['f','g']                    
        if head_cpos == 'V':
            if mod_is_f_g and head_deprel=='aux': result = 'xcomp'
            else:
                head_mod = head_feats_key_values.get('mod', None)
                head_mod_is_f_g = head_mod in ['f','g']
                if mod_is_f_g and not head_mod_is_f_g: result = 'xcomp'
                elif not mod_is_f_g and head_mod_is_f_g: result = 'ccomp'
                elif mod_is_f_g and head_mod_is_f_g: result = 'xcomp'
                else: # --> if not mod_is_f_g and not head_mod_is_f_g: 
                    num = form_feats_key_values.get('num', None)
                    per = form_feats_key_values.get('per', None)
                    head_num = head_feats_key_values.get('num', None)
                    head_per = head_feats_key_values.get('per', None)
                    num_per_agreement = num!=None and num==head_num and per!=None and per==head_per
                    result = 'xcomp' if num_per_agreement else 'ccomp'
        elif head_cpos in ['P', 'A', 'S', 'E', 'C', 'P', 'N', 'D', 'X', 'I', 'B', 'F', 'R', 'T']: result = 'xcomp'            
        else:
            return '{} 1:N arg (none of the conditions apply)'.format(ERROR_TAG)
    else:
        return '{} 1:N arg (none of the conditions apply)'.format(ERROR_TAG)
    if PRINT_WARNINGS: result += ' ({} 1:N arg)'.format(WARNING_TAG)
    return result


def get_udeprel_1_N_mod(cpos, head_cpos, dep_cpos):
    if cpos == 'A': result = 'amod'
    elif cpos == 'F': result = 'punct'
    elif cpos == 'B': result = 'advmod' 
    elif cpos == 'N': result = 'nummod' 
    elif cpos == 'V': result = 'advcl' if head_cpos == 'V' else 'dep' if head_cpos == 'F' else 'acl'
    elif cpos in ['C','E']: result = 'advcl' if dep_cpos == 'V' else 'advmod'
    elif cpos in ['N','D','T']: result = 'amod'
    elif cpos in ['P','S','R']: result = 'nmod'    
    elif cpos in ['I','X']: result = 'discourse'    
    else: return '{} 1:N mod (none of the conditions apply)'.format(ERROR_TAG)
    if PRINT_WARNINGS: result += '{} ( 1:N mod)'.format(WARNING_TAG)
    return result


def get_udeprel(id, head_id, lemma, deprel, head_deprel, cpos, head_cpos, form_feats_key_values, head_feats_key_values, dep_cpos):
    if head_id=='0':
        return 'root'
    if deprel == '_': #        
        assert '-' in id # underscore allowed only on split split tokens (3-4) e.g., prepart
        return '_'        
    if deprel in DEPREL_MAPPING:
        return DEPREL_MAPPING[deprel]
    if deprel == 'ROOT':
        return 'dep' # cannot be the root because head_id should be 0 (check first rule in this function)
    if deprel == 'arg': # 1:N args      
        return get_udeprel_1_N_arg(lemma, head_deprel, cpos, head_cpos, form_feats_key_values, head_feats_key_values)        
    if deprel == 'comp_ind': # 1:N comp_ind
        result = 'iobj' if cpos == 'P' else 'nmod'
        if PRINT_WARNINGS: result += ' ({} 1:N comp_ind)'.format(WARNING_TAG)
        return result
    if deprel in ['mod', 'mod_temp', 'mod_loc']: # 1:N mod*
        return get_udeprel_1_N_mod(cpos, head_cpos, dep_cpos)                
    if deprel == 'pred': # 1:N pred                
        if cpos == 'B': result = 'advmod'
        elif cpos == 'F': result = 'punct'
        elif cpos == 'I': result = 'discourse'        
        elif cpos in ['A','N']: result = 'amod'
        elif cpos in ['C','D','T','R','P','V','S','E']: result = 'nmod'
        elif cpos == 'X': result = 'dep' # general mapping case
        else: return '{} 1:N pred (none of the conditions apply)'.format(ERROR_TAG)
        if PRINT_WARNINGS: result += ' ({} 1:N pred)'.format(WARNING_TAG)
        return result
    if deprel == 'prep': # 1:N prep
        result = 'mark' if cpos == 'V' else 'case'
        if PRINT_WARNINGS: result += ' ({} 1:N prep)'.format(WARNING_TAG)
        return result
    return '{} (deprel:{})'.format(ERROR_TAG, deprel)

def get_key_values_feats(feats):
    if feats == '_': return {}
    result = {}
    for f in feats.split('|'):
        k,v = f.split('=')
        result[k] = v
    return result

def fix_multiple_roots(block):
    first_root_id = None
    head_index = CONLU_HEADER_INDEX['HEAD']
    for row in block:
        if row[head_index] == '0':
            if first_root_id == None:
                first_root_id = row[0]
            else:
                row[head_index] = first_root_id
                row[CONLU_HEADER_INDEX['DEPREL']] = 'parataxis'

def convert_block_paisa_conllu(old_rows):
    result = []
    for fields in old_rows:
        new_row = []
        new_row.extend(fields[:3]) # ID, FORM, LEMMA
        id = fields[CONLL_HEADER_INDEX['ID']]
        lemma = fields[CONLL_HEADER_INDEX['LEMMA']]
        cpos = fields[CONLL_HEADER_INDEX['CPOSTAG']]
        fpos = fields[CONLL_HEADER_INDEX['POSTAG']]
        feats = fields[CONLL_HEADER_INDEX['FEATS']]
        feats_key_values = get_key_values_feats(feats)
        head_id = fields[CONLL_HEADER_INDEX['HEAD']]
        deprel = fields[CONLL_HEADER_INDEX['DEPREL']]
        head_line = next((r for r in old_rows if r[CONLL_HEADER_INDEX['ID']]==head_id), None)        
        head_cpos = head_line[CONLL_HEADER_INDEX['CPOSTAG']] if head_line else '_'
        head_deprel = head_line[CONLL_HEADER_INDEX['DEPREL']] if head_line else '_'
        head_feats = head_line[CONLL_HEADER_INDEX['FEATS']] if head_line else '_'
        head_feats_key_values = get_key_values_feats(head_feats)        
        dep_line = next((r for r in old_rows if r[CONLL_HEADER_INDEX['HEAD']]==id), None)
        dep_cpos = dep_line[CONLL_HEADER_INDEX['CPOSTAG']] if dep_line else '_'
        new_row.append(get_upos(cpos, fpos)) # UPOS
        new_row.append(fpos) # XPOS
        new_row.append(get_ufeats(cpos, fpos, feats)) # FEATS
        new_row.append(head_id) # HEAD
        new_deprel = get_udeprel(id, head_id, lemma, deprel, head_deprel, cpos, head_cpos, feats_key_values, head_feats_key_values, dep_cpos) # DEPREL
        assert new_deprel != '_' or '-' in id
        new_row.append(new_deprel)
        new_row.append('_') # DEPS (unknown)
        new_row.append('_') # MISC (unknown) -> SpaceAfter=No
        assert len(new_row)==10
        result.append(new_row)
    fix_multiple_roots(result)
    return result

def convert_block_cupt_cupt(old_rows):
    result = []
    for fields in old_rows:
        new_row = []
        new_row.extend(fields[:3]) # ID, FORM, LEMMA
        id = fields[CUPT_HEADER_INDEX['ID']]
        lemma = fields[CUPT_HEADER_INDEX['LEMMA']]
        cpos = fields[CUPT_HEADER_INDEX['UPOS']]
        fpos = fields[CUPT_HEADER_INDEX['XPOS']]
        feats = fields[CUPT_HEADER_INDEX['FEATS']]
        feats_key_values = get_key_values_feats(feats)
        head_id = fields[CUPT_HEADER_INDEX['HEAD']]
        deprel = fields[CUPT_HEADER_INDEX['DEPREL']]
        deps = fields[CUPT_HEADER_INDEX['DEPS']]
        misc = fields[CUPT_HEADER_INDEX['MISC']]
        mwe = fields[CUPT_HEADER_INDEX['PARSEME:MWE']]
        head_line = next((r for r in old_rows if r[CUPT_HEADER_INDEX['ID']]==head_id), None)        
        head_cpos = head_line[CUPT_HEADER_INDEX['UPOS']] if head_line else '_'
        head_deprel = head_line[CUPT_HEADER_INDEX['DEPREL']] if head_line else '_'
        head_feats = head_line[CUPT_HEADER_INDEX['FEATS']] if head_line else '_'
        head_feats_key_values = get_key_values_feats(head_feats)
        dep_line = next((r for r in old_rows if r[CUPT_HEADER_INDEX['HEAD']]==id), None)
        dep_cpos = dep_line[CUPT_HEADER_INDEX['UPOS']] if dep_line else '_'
        new_row.append(get_upos(cpos, fpos)) # UPOS
        new_row.append(fpos) # XPOS
        new_row.append(get_ufeats(cpos, fpos, feats)) # FEATS
        new_row.append(head_id) # HEAD
        new_deprel = get_udeprel(id, head_id, lemma, deprel, head_deprel, cpos, head_cpos, feats_key_values, head_feats_key_values, dep_cpos) # DEPREL
        assert new_deprel != '_' or '-' in id
        new_row.append(new_deprel)
        new_row.append(deps) # DEPS
        new_row.append(misc) # MISC  -> SpaceAfter=No
        new_row.append(mwe) # MWE
        assert len(new_row)==11        
        result.append(new_row)    
    fix_multiple_roots(result)    
    return result    