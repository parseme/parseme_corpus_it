
def get_next_conll_header_and_block(f_in):
    headers = []
    block = []    
    for line in f_in:
        line_strip = line.strip()
        if line_strip == '': # empty line      
            if block:
                return headers, block
            else:
                continue # skip empty line
        if line_strip.startswith('#'):
            assert len(block) == 0 # make sure it's a header
            headers.append(line_strip)
            continue
        row = line_strip.split('\t')
        block.append(row)
    return headers, block    

def block_to_str(block):
    return '\n'.join(['\t'.join(fields) for fields in block])

def block_to_line_list(block):
    return ['\t'.join(fields) for fields in block]

def count_mwe_in_cupt_sentence(rows):
    from block_converter import CUPT_HEADER_INDEX
    mwe_num_set = set()
    for r in rows:
        mwe_field = r[CUPT_HEADER_INDEX['PARSEME:MWE']]
        for mwe in mwe_field.split(';'):
            if mwe not in ['*','_']:
                num = mwe.split(':')[0]
                assert int(num)>0
                mwe_num_set.add(num)    
    # if mwe_num_set:
    #     print(', '.join(mwe_num_set))
    return len(mwe_num_set)

def get_text_from_conllu(rows):
    from block_converter import CONLU_HEADER_INDEX
    from no_space_after import NO_SPACE_AFTER_TAG
    relevant_ids = set([r[0] for r in rows])
    # remove pre_art split
    id_to_remove = set()
    for id in relevant_ids:
        if '-' in id:
            id_to_remove.update(id.split('-'))
    relevant_ids -= id_to_remove
    space_after = lambda r: '' if r[CONLU_HEADER_INDEX['MISC']] == NO_SPACE_AFTER_TAG else ' '
    remaining_rows = [r for r in rows if r[0] in relevant_ids]
    spaces = [space_after(r) for r in remaining_rows]
    forms = [r[1] for r in remaining_rows]
    text = ''.join([item for pair in zip(forms, spaces) for item in pair])
    return text.strip()

def pass_head_consistency(block):
    from block_converter import CONLU_HEADER_INDEX
    head_index = CONLU_HEADER_INDEX['HEAD']
    last_row_num = len(block)
    for fields in block:
        if '-' in fields[0]:
            continue
        if int(fields[head_index]) > last_row_num:
            return False
    return True


# CONLU_HEADERS = 'ID FORM LEMMA UPOS XPOS FEATS HEAD DEPREL DEPS MISC'.split()
def compare_conllu(b1, b2, ignore_tags = []) :
    from block_converter import CONLU_HEADER_INDEX

    if len(b1)!= len(b2):
        print('Block differ in number of lines')
        return False
    relevan_conlu_head_indexes = {
        h:i for h,i in CONLU_HEADER_INDEX.items() 
        if h not in ignore_tags
    }
    for i in range(len(b1)):
        fields1 = b1[i][:10] # ignore 11th field (mwe) if present
        fields2 = b2[i][:10] # ignore 11th field (mwe) if present
        for h,i in relevan_conlu_head_indexes.items():
            if fields1[i] != fields2[i]:
                print('Lines differ in {}'.format(h))
                return False
    return True

if __name__ == '__main__':
    pass    