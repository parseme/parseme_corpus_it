import os
import json
import gzip
from tqdm import tqdm
import paisa_util
import conll_util
import prep_art
from paisa_util import PAISA_CONLL_FILE, NUM_SENTENCES_IN_PAISA
from block_converter import convert_block_paisa_conllu    
from block_converter import CONLL_HEADER_INDEX    
from no_space_after import add_no_space_after
import csv
from collections import defaultdict
from block_converter import ERROR_TAG, WARNING_TAG

def check_num_gen():
    pbar = tqdm(total=NUM_SENTENCES_IN_PAISA)
    pbar.set_description('Processing PAISA')
    num_exceptions = defaultdict(set)
    gen_exceptions = defaultdict(set)
    num_sentence = 0
    with gzip.open(PAISA_CONLL_FILE, 'rt') as f_in:
        while True:        
            _, block = paisa_util.get_next_paisa_header_and_block(f_in)
            if block == []: # EOF                
                break  

            for line in block:
                form = line[CONLL_HEADER_INDEX['FORM']]
                lemma = line[CONLL_HEADER_INDEX['LEMMA']]
                feats = line[CONLL_HEADER_INDEX['FEATS']]
                allowed_feat_values = {
                    'num': {
                        'allowed': ['s','p','n'],
                        'record': num_exceptions
                    },
                    'gen': {
                        'allowed': ['m','f','n'],
                        'record': gen_exceptions
                    }
                }
                for f in feats.split('|'):
                    for k,v in allowed_feat_values.items():
                        if f.startswith(k): # num/gen
                            feat_value = f.split('=')[1]
                            if feat_value not in v['allowed']:
                                v['record']['{}|{}'.format(form,lemma)].add(f)
            pbar.update()
            num_sentence += 1
            # if  num_sentence==100000:
            #     break
        pbar.close()  

    num_exceptions = {k:sorted(v) for k,v in num_exceptions.items()}
    gen_exceptions = {k:sorted(v) for k,v in gen_exceptions.items()}
    
    print(json.dumps(num_exceptions, ensure_ascii=False, indent=3))
    print(json.dumps(gen_exceptions, ensure_ascii=False, indent=3))

def check_num_cols_in_paisa():
    pbar = tqdm(total=NUM_SENTENCES_IN_PAISA)
    pbar.set_description('Processing PAISA')
    with gzip.open(PAISA_CONLL_FILE, 'rt') as f_in:
        while True:        
            _, block = paisa_util.get_next_paisa_header_and_block(f_in)
            if block == []: # EOF                
                break  
            for line in block:
                if len(line)!=8:
                    pass
            pbar.update()
        pbar.close()   

def build_raw_corpus(split_prep_art=True, discard_annotated=False, gzipped=True, limit=None):

    current_text_id, current_text_part, current_url = None, None, None

    output_dir = './st_1_2_raw'
    sentences_per_file = 50000
    current_file_id = 1

    pbar = tqdm(total=NUM_SENTENCES_IN_PAISA)
    pbar.set_description('Processing PAISA')
    num_errors, num_sentence, converted, discarded_annotated_num, inconsisten_head_num = 0, 0, 0, 0, 0

    log_tsv_output_file = './log_conversion.tsv' 
    tsv_out = open(log_tsv_output_file, 'w')
    tsv_writer = csv.writer(tsv_out, delimiter='\t')

    get_file_name = lambda id: ('raw-{}.conllu.gz' if gzipped else 'raw-{}.conllu').format(str(id).zfill(3))
    output_open_command = gzip.open if gzipped else open
    get_f_out = lambda fid: output_open_command(os.path.join(output_dir, get_file_name(fid)), 'wt')

    f_out = get_f_out(current_file_id)

    discarded_text_id = None
    if discard_annotated:
        discarded_text_id = set()
        for part in ['train','dev','test']:
            mapping_file = os.path.join('st_1_1_paisa_mapping', '{}_mapping.json'.format(part))
            with open(mapping_file) as f_in:
                mapping = json.load(f_in)
                for v in mapping.values():
                    text_id_field = v.split()[3].split(':')
                    assert text_id_field[0]=='sent_id'
                    discarded_text_id.add(text_id_field[1])


    with gzip.open(PAISA_CONLL_FILE, 'rt') as f_in:
        
        while True:        
            header, block = paisa_util.get_next_paisa_header_and_block(f_in)

            if block == []: # EOF
                f_out.write('\n')
                break

            ## conversion
            converted_block = prep_art.split_prep_art(block) if split_prep_art else block
            converted_block = convert_block_paisa_conllu(converted_block)
            add_no_space_after(converted_block)                
            ## end of conversion

            pbar.update()
            num_sentence += 1

            if not conll_util.pass_head_consistency(converted_block):
                inconsisten_head_num += 1
                with open('./inconsistent_raw.log', 'a') as f_log_out:
                    f_log_out.write(conll_util.block_to_str(converted_block))
                    f_log_out.write('\n\n')
                continue

            if num_sentence % sentences_per_file == 0:
                f_out.close()
                current_file_id += 1
                f_out = get_f_out(current_file_id)

            if header == None: # no section information
                current_text_part += 1
            else:
                current_text_id, current_url = paisa_util.get_text_id_url_from_header(header)
                current_text_part = 1

            sentence_text = conll_util.get_text_from_conllu(converted_block)
            text_id_part = '{}_{}'.format(current_text_id, current_text_part)
            if discarded_text_id and text_id_part in discarded_text_id:
                discarded_annotated_num += 1
                continue

            # f_out.write('# PAISA sent_id:{} url:{}\n'.format(text_id_part, current_url))
            f_out.write('# sent_id = {}\n'.format(text_id_part))
            f_out.write('# text = {}\n'.format(sentence_text.strip()))        
            converted_block.extend([[],[]]) # two empty lines at the end
            converted_block_str = conll_util.block_to_str(converted_block)
            f_out.write(converted_block_str)        
            
            converted += 1
            if converted == limit:
                break

            if ERROR_TAG in converted_block_str:
                num_errors += 1
                for i in range(len(block)):
                    tsv_row = converted_block[i] + ['⇐'] + block[i]
                    tsv_writer.writerow(tsv_row)
                tsv_writer.writerow([])
                if  num_errors==1:
                    break  

    pbar.close()    
    f_out.close()
    tsv_out.close()

    print("Total sentences: {}".format(num_sentence))
    print("Number of errors: {}".format(num_errors))
    print("Converted: {}".format(converted))
    print("Discarded (annotated): {}".format(discarded_annotated_num))
    print("Discarded (wrong heads): {}".format(inconsisten_head_num))    


if __name__ == '__main__':
    # check_num_cols_in_paisa()
    # check_num_gen()

    # build_raw_corpus(split_prep_art=True, discard_annotated=True, gzipped=True, limit=None)
    # python validate.py --lang=ud --level=2 ./st_1_2_raw/raw-001.conllu1000

    build_raw_corpus(split_prep_art=True, discard_annotated=True)

    